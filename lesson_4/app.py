import datetime
import traceback

from lesson_3.models.positions import Candindate
from lesson_4.custom_exceptions import UnableToWorkException


def main():
    cand = Candindate()
    cand.work()


if __name__ == '__main__':
    try:
        main()
    except Exception as err:
        with open('logs.txt', 'a') as f:
            message = '{}    {}:\n {} \n\n'.format(
                datetime.datetime.now(),
                err.__class__.__name__,
                traceback.format_exc()
            )
            f.write(message)
        print('Error was logged!')
