class A:
    def __init__(self):
        print("A")


class B(A):
    def __init__(self):
        super().__init__()
        print("B")


class C(A):
    def __init__(self):
        super().__init__()
        print("C")


class D(B, C):
    def __init__(self):
        super().__init__()
        print("D")


class E(D, B):
    def __init__(self):
        super().__init__()
        # CTRL + D
        # print("E")
        print("E + R")

#
# L[SomeClass] = SomeClass + merge(L[parent1],.., L[parentN], [parent1,.., parentN])
#
# L[A] = A
# L[B] = B + merge(L[A], [A]) = B + merge([A], [A]) = B + A + merge([], []) = B, A
# L[C] = C, A
# L[D] = D + merge([L(B), L(C), [B, C]]) = D + merge([B, A], [C, A], [B, C]) =
#      = D + B + merge([A], [C, A], [C]) = D + B + C + A
#
# # D & B
# L[E] = E + merge([L[D], L[B], [D, B]]) = E + merge([D, B, C, A], [B, A], [D, B]) =
#     = E + D + merge([B, C, A], [B, A], [B]) = E + D + B + merge([C, A], [A]) =
#     = E + D + B + C + A
