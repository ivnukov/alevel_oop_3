"""
Parent class lives here.
"""
from datetime import datetime


class Parent:
    """
    Parent class
    """
    def __init__(self, property_1, property_2):
        self.property_1 = property_1
        self.property_2 = property_2

    def print_place(self):
        """
        just for print where I am
        :return: None
        """
        print('Called from parent.')

    def __str__(self):
        return "{}: {}".format(self.__class__.__name__,
                               self.property_1)

    def __call__(self, param1, param2):
        return param1 * param2

    def __del__(self):
        print(datetime.now())


class Child(Parent):
    def __init__(self, property_1, property_2, child_property):
        super().__init__(property_1, property_2)
        self.child_property = child_property

    def print_place(self):
        print('From child')

    def __lt__(self, other):
        print("LT")
        return self.property_1 < other.property_1

    def __gt__(self, other):
        print("GT")
        return self.property_1 > other.property_1

    def __eq__(self, other):
        print("EQ")
        return self.property_1 == other.property_1

    def __le__(self, other):
        return self.property_1 <= other.property_1

    def __ge__(self, other):
        return self.property_1 >= other.property_1

    def __ne__(self, other):
        return self.property_1 != other.property_1

    def __add__(self, other):
        self.property_1 += other.property_1
        return self.property_1
