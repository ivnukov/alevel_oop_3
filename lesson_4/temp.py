from datetime import datetime

from lesson_4.custom_exceptions import MyAwesomeException


class TestClass:
    a = 5


def main():
    # a = input()
    # if len(a) < 5:
    #     raise MyAwesomeException
    #
    # assert len(a) > 6
    obj = TestClass()
    print(obj.a)
    print(hasattr(obj, 'b'))
    print(getattr(obj, 'b', "TEST"))
    # input_ = None
    # if input_ == 'a':
    #     return obj.a
    # if input_ == 'b':
    #     return obj.b
    # getattr(obj, input_, 'None')

    setattr(obj, 'b', datetime.now())
    obj.b = datetime.now()
    print(obj.b)

    some_list = [1, 2, 3, 4, 5]
    # print(some_list[8])
    some_dict = dict(a=1, b=2, c=3)
    # print(some_dict['d'])
    print(some_dict.get('d', "Default"))

    if some_dict['a'] > 1:
        what = 'Is love'
    else:
        what = 'baby dont hurt me'
    print(what)


if __name__ == '__main__':
    main()
