class Record:

    def __init__(self, phone_number, address=None):
        self.phone_number = phone_number  # this should be unique
        self.address = address

    def __str__(self):
        raise NotImplementedError

    @classmethod
    def from_csv(cls, fp):
        raise NotImplementedError


class Person(Record):
    def __init__(self, first_name, last_name, email, phone_number, address=None):
        super().__init__(phone_number, address)
        self.first_name = first_name
        self.last_name = last_name
        self.email = email  # this should be unique

    @classmethod
    def from_csv(cls, fp):
        pass


class Organization(Record):
    def __init__(self, name, category, phone_number, address):
        super().__init__(phone_number, address)
        self.name = name  # this should be unique
        self.category = category

    @classmethod
    def from_csv(cls, fp):
        pass


class AddressBook:
    def __init__(self, fp):
        self.fp = fp

    def validate_person(self, data):
        pass

    def validate_org(self, data):
        pass

    def add_record(self, type_, data):
        pass

    def find_record(self, type_, search_term):
        pass

    def get_records(self, type_):
        pass

    def import_from_csv(self, fp):
        pass
