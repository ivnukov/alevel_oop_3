class Element:
    def __init__(self, t_melt, t_vapor):
        self.t_melt = t_melt
        self.t_vapor = t_vapor

    @staticmethod
    def convert_to_c(temp, scale):
        if scale == 'K':
            temp = temp - 273
        if scale == 'F':
            temp = 12345
        return temp

    def agg_state(self, temp, scale="C"):
        # extra logic to convert temp

        temp = self.convert_to_c(temp, scale)

        if temp < self.t_melt:
            return "SOLID"
        if temp >= self.t_vapor:
            return "GAS"
        return "LIQUID"


Element.convert_to_c(23, 'K')


def convert_to_c(temp, scale):
    if scale == 'K':
        temp = temp - 273
    if scale == 'F':
        temp = 12345
    return temp
