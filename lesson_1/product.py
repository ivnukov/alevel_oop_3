CATEGORIES = ['eat', 'chem', 'alcohol']


class Product:
    def __init__(self, name, price, cat, unit):
        self.name = name
        self.price = price
        self.cat = cat
        self.unit = unit

    def is_eatable(self):
        return self.cat != 'chem'

    def price_total(self, unit):
        return self.price * unit


class Eat(Product):

    def is_eatable(self):
        return True


class Cart:
    def __init__(self, product_and_amount):
        self.product_and_amount = product_and_amount

    def total(self):
        total = 0
        for product, amount in self.product_and_amount:
            total += product.price_total(amount)
        return total

    def totally_eatable(self):
        return all([x[0].is_eatable for x in self.product_and_amount])


bread = Product('bread', 10, 'eat', 'шт')
meat = Product('meat', 10, 'eat', 'шт')
toilet_paper = Product('tiolet_paper', 13, 'chem', 'шт')

cart = Cart([[bread, 10], [toilet_paper, 50]])
