from lesson_1.element import Element


def run():
    iron = Element(t_melt=1538, t_vapor=2862)
    oxygen = Element(t_melt=-218, t_vapor=-182)
    print(iron.agg_state(1600, 'K'))
    print(oxygen.agg_state(-150))
    print(iron.convert_to_c(10000, 'F'))


if __name__ == '__main__':
    run()
